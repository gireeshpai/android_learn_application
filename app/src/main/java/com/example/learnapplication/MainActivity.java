package com.example.learnapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    private ImageButton b_findtheday, b_calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Tool Box"); //CHANGE THE ACTIVITY TITLE
        setContentView(R.layout.activity_main);

        b_calculator = findViewById(R.id.b_calculator);

        b_calculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calculator = new Intent(getApplicationContext(), CalculatorActivity.class);
                startActivity(calculator);
            }
        });

        b_findtheday = findViewById(R.id.b_findtheday);

        b_findtheday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findtheday = new Intent(getApplicationContext(), FindTheDay.class);
                startActivity(findtheday);
            }
        });
    }
}
