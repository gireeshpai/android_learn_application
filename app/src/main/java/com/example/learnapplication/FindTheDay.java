package com.example.learnapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FindTheDay extends AppCompatActivity {

    private Spinner s_day,s_month, s_year;
    private Button b_find;
    private TextView l_answser;

    static final int[] month_code = new int[]{1, 4, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6};
    static final int[] century_code = new int[]{0, 6, 4, 2};
    static final int initial_year = 1500;   //CHANGE THIS TO APPLY EVERYWHERE
    static final int final_year = 2100;   //CHANGE THIS TO APPLY EVERYWHERE
    static final String[] weekdays = new String[]{"Saturday", "Sunday", "Monday", "Tuesday",
            "Wednesday", "Thursday", "Friday"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Find the day"); //CHANGE THE ACTIVITY TITLE
        setContentView(R.layout.activity_find_the_day);

        s_day = findViewById(R.id.s_day);
        s_month = findViewById(R.id.s_month);
        s_year = findViewById(R.id.s_year);

        b_find = findViewById(R.id.b_find);

        l_answser = findViewById(R.id.l_answser);

        //INITIALIZING THE LISTS FOR DAY, MONTH AND YEAR
        List<Integer> spinner_day = new ArrayList<Integer>();
        for(int index=1;index<=31;index++)
        {
            spinner_day.add(index);
        }

        List<Integer> spinner_month = new ArrayList<Integer>();
        for(int index=1; index<=12; index++)
        {
            spinner_month.add(index);
        }

        List<Integer> spinner_year = new ArrayList<Integer>();
        for(int index=initial_year; index<=final_year; index++)
        {
            spinner_year.add(index);
        }


        //INITIALIZING ARRAY ADAPTER
        ArrayAdapter<Integer> day_adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, spinner_day);

        ArrayAdapter<Integer> month_adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, spinner_month);

        ArrayAdapter<Integer> year_adapter = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_spinner_dropdown_item, spinner_year);


        //SETTING ARRAY ADAPTER
        day_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_day.setAdapter(day_adapter);

        month_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_month.setAdapter(month_adapter);

        year_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_year.setAdapter(year_adapter);


        s_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        b_find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String calculated_day = getDay();
                l_answser.setText(calculated_day);
            }
        });
    }


    private String getDay()   //MAIN LOGIC
    {
        int day = (int)s_day.getSelectedItem();
        int month = (int)s_month.getSelectedItem();
        int year = (int)s_year.getSelectedItem();

        int r_century_code = 0;
        int r_month_code = month_code[month-1];

        int delta_century = ((year/100)-(initial_year/100)) / 5;


        if (delta_century <= 4)
        {
            r_century_code = century_code[delta_century];
        }
        else
        {
            int temp = 0 + (delta_century-4);
            r_century_code = century_code[temp];
        }

        int year_end = year % 100;

        int no_leap_years = noOfLeapYears(year);
        String required_day;

        int pre_compute = day + r_month_code + r_century_code + year_end + no_leap_years;

        if(day<=29 && month<=2)
        {
            pre_compute = pre_compute - 1;
            int day_number = pre_compute%7;
            Log.e("DAY NUM", "DAY NUM : "+day_number);

            if(getLeapYear(year))
                required_day = weekdays[day_number-1];
            else
                required_day = weekdays[day_number];


//            if(getLeapYear(year))
//                required_day = weekdays[day_number-1];
//            else
//                required_day = weekdays[day_number];

//            if(getLeapYear(year))
//                if(day_number-1 < 0)
//                    required_day = weekdays[6];
//                else
//                    required_day = weekdays[day_number-1];
//            else
//                required_day = weekdays[day_number];
//            required_day = weekdays[day_number-1];
        }


        else
        {
//            Log.e("PRE_COMPUTE", "PRE COMPUTE = "+pre_compute);
            int day_number = pre_compute%7;
//            required_day = weekdays[day_number];

            if(getLeapYear(year)) {
                Log.e("DAY NUMBER", "DAY NUMBER= " + day_number);
                if(day_number == 0)//ISSUE WITH THIS LOGIN WHEN JAN 30, day is greater but month < 2
                    required_day = weekdays[5];
                else
                    required_day = weekdays[day_number-1];
            }
            else
            {
                if(day_number-1 < 0)
                    required_day = weekdays[6];
                else
                    required_day = weekdays[day_number-1];
            }
//            else
//                required_day = weekdays[day_number-1];
        }
//        Toast.makeText(FindTheDay.this, "You have selected : "+s_day.getSelectedItem(), Toast.LENGTH_LONG).show();
        return(required_day);
    }



    private boolean getLeapYear(int year)   //LEAP YEAR CHECK
    {
        boolean leap_year = false;
        if(((year % 4==0) && (year % 100!=0)) || year % 400==0)
        {
            leap_year = true;
        }
        else
        {
            leap_year = false;
        }
        return(leap_year);
    }


    private int noOfLeapYears(int year) //COUNTING THE NO:OF LEAP YEARS
    {
        int century = (year / 100) * 100;
        int no_leap_years = 0;
        for (int index = century; index<=year; index+=4)
        {
            if(getLeapYear(index))
            {
                no_leap_years++;
            }
        }
        return (no_leap_years);
    }
}
